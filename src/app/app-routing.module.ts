import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuardService } from './services/auth-guard/auth-guard.service';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ProfileComponent } from './components/profile/profile.component';
import { InvoicesComponent } from './components/invoices/invoices.component';
import { LoginComponent } from './components/login/login.component';
import { VehiclesComponent } from './components/vehicles/vehicles.component';
import { RatesComponent } from './components/rates/rates.component';
import { AccountsComponent } from './components/accounts/accounts.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [ AuthGuardService ],
    children: [
      {
        path: '',
        redirectTo: '/dashboard/(dashboard:vehicles)',
        pathMatch: 'full'
      },
      {
        path: 'profile',
        component: ProfileComponent,
        outlet: 'dashboard'
      },
      {
        path: 'invoices',
        component: InvoicesComponent,
        outlet: 'dashboard'
      },
      {
        path: 'rates',
        component: RatesComponent,
        outlet: 'dashboard'
      },
      {
        path: 'vehicles',
        component: VehiclesComponent,
        outlet: 'dashboard'
      },
      {
        path: 'accounts',
        component: AccountsComponent,
        outlet: 'dashboard'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
