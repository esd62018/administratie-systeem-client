import { Component, OnInit } from '@angular/core';
import { AccountService } from './services/account/account.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(
    private accountService: AccountService
  ) {}

  ngOnInit(): void {
    this.accountService.authenticate();
  }
}
