import { NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { MaterialModule } from './modules/material/material.module';

import { AppComponent } from './app.component';
import { SnackBarComponent } from './components/snack-bar/snack-bar.component';
import { LoaderComponent } from './components/loader/loader.component';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ProfileComponent } from './components/profile/profile.component';
import { VehiclesComponent } from './components/vehicles/vehicles.component';
import { VehicleFormComponent } from './components/vehicle-form/vehicle-form.component';
import { InvoicesComponent } from './components/invoices/invoices.component';
import { InvoiceTableComponent } from './components/invoice-table/invoice-table.component';
import { RatesComponent } from './components/rates/rates.component';
import { RateFormComponent } from './components/rate-form/rate-form.component';
import { RateComponent } from './components/rate/rate.component';
import { AccountsComponent } from './components/accounts/accounts.component';

import { HttpClientService } from './services/http-client/http-client.service';
import { AccountService } from './services/account/account.service';
import { VehiclesService } from './services/vehicles/vehicles.service';
import { InvoicesService } from './services/invoices/invoices.service';
import { AuthGuardService } from './services/auth-guard/auth-guard.service';
import { SnackBarService } from './services/snack-bar/snack-bar.service';
import { LoaderService } from './services/loader/loader.service';
import { RatesService } from './services/rates/rates.service';
import { AccountsService } from './services/accounts/accounts.service';

@NgModule({
  declarations: [
    AppComponent,
    SnackBarComponent,
    LoaderComponent,
    LoginComponent,
    DashboardComponent,
    ProfileComponent,
    VehiclesComponent,
    VehicleFormComponent,
    InvoicesComponent,
    InvoiceTableComponent,
    RatesComponent,
    RateFormComponent,
    RateComponent,
    AccountsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    HttpClientService,
    AccountService,
    VehiclesService,
    InvoicesService,
    AuthGuardService,
    SnackBarService,
    LoaderService,
    RatesService,
    AccountsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
