import {Component, OnInit} from '@angular/core';

import {SnackBarService} from '../../services/snack-bar/snack-bar.service';
import {LoaderService} from '../../services/loader/loader.service';
import {AccountsService} from '../../services/accounts/accounts.service';
import {GovernmentAccount} from '../../models/GovernmentAccount';
import {AccountService} from '../../services/account/account.service';
import {Account} from '../../models/Account';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.scss']
})
export class AccountsComponent implements OnInit {
  accounts: GovernmentAccount[] = [];
  auth: Account = null;
  username: String;

  constructor(
    private snackBarService: SnackBarService,
    private loaderService: LoaderService,
    private accountsService: AccountsService,
    private accountService: AccountService
  ) { }

  ngOnInit() {
    this.getAccounts();
    this.auth = this.accountService.auth;
    const {username} = this.auth;
    this.username = username;
  }

  getAccounts(): void {
    this.loaderService.showLoader();
    this.accountsService
      .getAll()
      .subscribe(
        accounts => this.accounts = accounts,
        error => this.onError(error),
        () => this.onComplete()
      );
  }

  onError(error: any): void {
    const { status } = error;
    if (status && status === 500) {
      this.snackBarService.showOops();
    }

    this.loaderService.hideLoader();
  }

  onComplete(): void {
    this.loaderService.hideLoader();
  }

  updateRole(account: Account, role: string): void {
    account.role = role;
    this.accountsService
      .update(account)
      .subscribe(
        error => this.onError(error),
        () => this.onComplete()
      );
  }
}
