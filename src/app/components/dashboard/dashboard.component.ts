import { Component, OnInit } from '@angular/core';

import { AccountService } from '../../services/account/account.service';
import {Account} from '../../models/Account';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  auth: Account = null;
  role: String;

  constructor(
    private accountService: AccountService
  ) { }

  ngOnInit() {
    this.auth = this.accountService.auth;
    const { role } = this.auth;
    this.role = role;
  }

  logout(): void {
    this.accountService.logout();
  }
}
