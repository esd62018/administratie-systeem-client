import { Component, OnInit } from '@angular/core';

import { InvoicesService } from '../../services/invoices/invoices.service';
import { SnackBarService } from '../../services/snack-bar/snack-bar.service';
import { LoaderService } from '../../services/loader/loader.service';

import { Invoice } from '../../models/Invoice';

import { invoiceSorter } from '../../utils/Sort';
import {MatExpansionPanel} from '@angular/material';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.scss']
})
export class InvoicesComponent implements OnInit {

  invoices: Invoice[] = [];

  indexOpenedInvoice: number = -1;

  constructor(
    private invoiceService: InvoicesService,
    private snackBarService: SnackBarService,
    private loaderService: LoaderService
  ) { }

  ngOnInit(): void {
    this.getInvoices();
  }

  setOpenedInvoice(number: number) {
    this.indexOpenedInvoice = number;
  }

  getInvoices(): void {
    this.loaderService.showLoader();

    this.invoiceService
      .getAllInvoices()
      .subscribe(
        invoices => this.invoices = invoices.sort(invoiceSorter),
        error => this.onError(error),
        () => this.onComplete()
      );
  }

  onError(error: any): void {
    const { status } = error;
    if (status && status === 500) {
      this.snackBarService.showOops();
    }

    this.loaderService.hideLoader();
  }

  onComplete(): void {
    this.loaderService.hideLoader();
  }

  generateInvoices(): void {
    this.invoiceService
      .generateInvoices('01-07-2018')
      .subscribe(
        generatedInvoices => this.getInvoices(),
        error => this.onError(error),
        () => this.onComplete()
      );
  }

  changePaymentStatus(invoice: Invoice, paymentStatus: string, event: Event): void {
    event.stopPropagation();
    invoice.paymentStatus = paymentStatus;
    this.invoiceService
      .update(invoice)
      .subscribe(
        error => this.onError(error),
        () => this.onComplete()
      );
  }
}
