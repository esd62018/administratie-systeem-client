import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

import { AccountService } from '../../services/account/account.service';
import { SnackBarService } from '../../services/snack-bar/snack-bar.service';
import { LoaderService } from '../../services/loader/loader.service';
import { Account } from '../../models/Account';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  usernameFormControl = new FormControl('', [
    Validators.required
  ]);

  passwordFormControl = new FormControl('', [
    Validators.required
  ]);

  submitError: string = null;

  constructor(
    private accountService: AccountService,
    private snackBarService: SnackBarService,
    private loaderService: LoaderService
  ) { }

  ngOnInit() {

  }

  submit(): void {
    if (this.usernameFormControl.invalid || this.passwordFormControl.invalid) {
      return;
    }

    const username: string = this.usernameFormControl.value;
    const password: string = this.passwordFormControl.value;
    const account: Account = new Account({ username, password });

    this.loaderService.showLoader();

    this.accountService
      .login(account)
      .subscribe(
        () => {},    // Ignore (is handled by InvoicesService)
        error => this.onError(error),
        () => this.onComplete()
      );
  }

  onError(error: any): void {
    const { status } = error;
    if (status && status === 404) {
      this.submitError = 'Invalid username or password';
    } else {
      this.snackBarService.showOops();
    }

    this.loaderService.hideLoader();
  }

  onComplete(): void {
    this.loaderService.hideLoader();
  }
}
