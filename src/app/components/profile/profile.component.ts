import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

import { AccountService } from '../../services/account/account.service';
import { SnackBarService } from '../../services/snack-bar/snack-bar.service';
import { LoaderService } from '../../services/loader/loader.service';
import { Account } from '../../models/Account';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  roles: string[] = ['LIMITED', 'KM_PRIJS', 'ADMIN'];

  submitError: string = null;

  account: Account = null;

  role: String;

  usernameFormControl = new FormControl('', [
    Validators.required
  ]);

  passwordFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(6),
  ]);

  roleFormControl = new FormControl('', [
    Validators.required
  ]);

  constructor(
    private accountService: AccountService,
    private snackBarService: SnackBarService,
    private loaderService: LoaderService
  ) {
    this.account = this.accountService.auth;
    const { role } = this.account;
    this.role = role;
  }

  ngOnInit() {
    this.mapAccountToFormControlValues();
  }

  mapAccountToFormControlValues(): void {
    if (!this.account) {
      return;
    }

    const { username, password, role } = this.account;

    this.usernameFormControl.setValue(username);
    this.passwordFormControl.setValue(password);
    this.roleFormControl.setValue(role);
  }

  submit(): void {
    if (this.usernameFormControl.invalid ||
        this.passwordFormControl.invalid ||
        this.roleFormControl.invalid
    ) {
      return;
    }

    const username: string = this.usernameFormControl.value;
    const password: string = this.passwordFormControl.value;
    const role: string = this.roleFormControl.value;

    const account: Account = new Account({
      id: this.account.id,
      username,
      password,
      role
    });

    this.loaderService.showLoader();

    this.accountService
      .update(account)
      .subscribe(
        () => this.snackBarService.showSnackBar('Gegevens succesvol aangepast'),
        error => this.onError(error),
        () => this.onComplete()
      );
  }

  onError(error: any): void {
    const { status } = error;
    if (status && status === 404) {
      this.submitError = 'Invalid username or password';
    } else {
      this.snackBarService.showOops();
    }

    this.loaderService.hideLoader();
  }

  onComplete(): void {
    this.loaderService.hideLoader();
  }
}
