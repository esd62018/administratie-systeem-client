import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroupDirective, FormControl, Validators} from '@angular/forms';
import moment = require('moment');

import { Account } from '../../models/Account';
// import { Rate } from '../../models/Rate';

@Component({
  selector: 'app-rate-form',
  templateUrl: './rate-form.component.html',
  styleUrls: ['./rate-form.component.scss']
})
export class RateFormComponent implements OnInit {
  categories: string[] = ['GASOLINE', 'DIESEL', 'ELECTRICAL'];

  regions: string[] = ['Vlaanderen', 'Wallonië'];

  @Input() isDisabled: boolean = false;

  @Input() resetAfterSubmit: boolean = false;

  @Input() submitButtonText: string = null;

  @Input() showLoginButton: boolean = null;

  @Input() submitError: string = null;

  @Input() rate = null;

  @Output() parentSubmitCallback = new EventEmitter<Account>();

  regionFormControl = new FormControl('', [
    Validators.required
  ]);

  categoryFormControl = new FormControl('', [
    Validators.required
  ]);

  startDateFormControl = new FormControl('', [
    Validators.required
  ]);

  endDateFormControl = new FormControl('', [
    Validators.required
  ]);

  priceFormControl = new FormControl('', [
    Validators.required,
    Validators.min(0.01)
  ]);

  constructor() { }

  ngOnInit() {
    this.mapRateToFormControlValues();
  }

  mapRateToFormControlValues(): void {
    if (!this.rate) {
      return;
    }

    const {
      region,
      rateCategory,
      startDate,
      endDate,
      ratePrice
    } = this.rate

    this.regionFormControl.setValue(region);
    this.categoryFormControl.setValue(rateCategory);
    this.startDateFormControl.setValue(moment(startDate).toISOString());
    this.endDateFormControl.setValue(moment(endDate).toISOString());
    this.priceFormControl.setValue(ratePrice);

    if (this.isDisabled) {
      this.regionFormControl.disable();
      this.categoryFormControl.disable();
      this.startDateFormControl.disable();
      this.endDateFormControl.disable();
      this.priceFormControl.disable();
    }
  }

  submit(formDirective: FormGroupDirective): void {
    if (
      this.regionFormControl.invalid ||
      this.categoryFormControl.invalid ||
      this.startDateFormControl.invalid ||
      this.endDateFormControl.invalid ||
      this.priceFormControl.invalid
    ) {
      return;
    }

    const region = this.regionFormControl.value;
    const category = this.categoryFormControl.value;
    const startDate = moment(this.startDateFormControl.value).format('MM-DD-YYYY');
    const endDate = moment(this.endDateFormControl.value).format('MM-DD-YYYY');
    const price = this.priceFormControl.value;

    // Note: should be rate = new Rate({ ... }) but Rate isn't usable..
    const rate = {
      id: this.rate ? this.rate.id : null,
      region,
      startDate,
      endDate,
      rateCategory: category,
      ratePrice: price
    };

    if (this.resetAfterSubmit) {
      formDirective.resetForm();
      this.regionFormControl.reset();
      this.categoryFormControl.reset();
      this.startDateFormControl.reset();
      this.endDateFormControl.reset();
      this.priceFormControl.reset();
    }

    this.parentSubmitCallback.emit(rate);
  }
}
