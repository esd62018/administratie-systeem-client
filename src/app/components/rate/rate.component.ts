import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Rate } from '../../models/Rate'
import {Account} from '../../models/Account';
import {AccountService} from '../../services/account/account.service';

@Component({
  selector: 'app-rate',
  templateUrl: './rate.component.html',
  styleUrls: ['./rate.component.scss']
})
export class RateComponent implements OnInit {
  isEditing: boolean = false;
  auth: Account = null;
  role: String;

  @Input() rate: Rate = null;

  @Output() updateCallback = new EventEmitter<Rate>();

  @Output() deleteCallback = new EventEmitter<number>();

  constructor(
    private accountService: AccountService
  ) { }

  ngOnInit() {
    this.auth = this.accountService.auth;
    const { role } = this.auth;
    this.role = role;
  }

  toggleEditing(): void {
    this.isEditing = !this.isEditing;
  }

  onUpdate(rate: Rate) {
    this.updateCallback.emit(rate);
    this.isEditing = false;
  }

  onDelete(): void {
    this.deleteCallback.emit(this.rate.id);
  }
}
