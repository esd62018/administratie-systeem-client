import { Component, OnInit } from '@angular/core';


import { RatesService } from '../../services/rates/rates.service';
import { SnackBarService } from '../../services/snack-bar/snack-bar.service';
import { LoaderService } from '../../services/loader/loader.service';
import { AccountService } from '../../services/account/account.service';

import { Account } from '../../models/Account';
import { Rate } from '../../models/Rate';

import { ratesSorter } from '../../utils/Sort';

@Component({
  selector: 'app-rates',
  templateUrl: './rates.component.html',
  styleUrls: ['./rates.component.scss']
})
export class RatesComponent implements OnInit {
  rates: Rate[] = [];
  auth: Account = null;
  role: String;

  constructor(
    private ratesService: RatesService,
    private snackBarService: SnackBarService,
    private loaderService: LoaderService,
    private accountService: AccountService
  ) { }

  ngOnInit() {
    this.getRates();
    this.auth = this.accountService.auth;
    const { role } = this.auth;
    this.role = role;
  }

  getRates(): void {
    this.loaderService.showLoader();

    this.ratesService
      .getAllRates()
      .subscribe(
        rates => this.rates = rates,
        error => this.onError(error),
        () => this.onComplete()
      );
  }

  submitNew(rate: Rate): void {
    this.ratesService
      .addNewRate(rate)
      .subscribe(
        newRate => this.rates.unshift(newRate),
        error => this.onError(error),
        () => this.onComplete()
      );
  }

  updateRate(rate: Rate): void {
    this.ratesService
      .updateRate(rate)
      .subscribe(
        updatedRate => {
          const index = this.rates.findIndex(r => r.id === rate.id);
          const rates = this.rates;
          rates[index] = updatedRate;
          this.rates = rates;
        },
        error => this.onError(error),
        () => this.onComplete()
      );
  }

  deleteRate(id: number): void {
    this.ratesService
      .deleteRate(id)
      .subscribe(
        () => this.rates = this.rates.filter(r => r.id !== id).sort(ratesSorter),
        error => this.onError(error),
        () => this.onComplete()
      );
  }

  onError(error: any): void {
    const { status } = error;
    if (status && status === 500) {
      this.snackBarService.showOops();
    }

    this.loaderService.hideLoader();
  }

  onComplete(): void {
    this.loaderService.hideLoader();
  }
}
