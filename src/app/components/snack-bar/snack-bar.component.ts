import { Component, OnInit } from '@angular/core';
import { SnackBarService } from '../../services/snack-bar/snack-bar.service';

@Component({
  selector: 'app-snack-bar',
  templateUrl: './snack-bar.component.html',
  styleUrls: ['./snack-bar.component.scss']
})
export class SnackBarComponent implements OnInit {
  public message: string;

  constructor(
    private service: SnackBarService
  ) { }

  ngOnInit() {
    this.service.message.subscribe((newMessage?: string) => this.message = newMessage);
  }
}
