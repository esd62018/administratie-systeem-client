import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroupDirective, FormControl, Validators} from '@angular/forms';
import moment = require('moment');

import { AccountService } from '../../services/account/account.service';
import { VehicleHistory } from '../../models/VehicleHistory';
import { Vehicle } from '../../models/Vehicle';

@Component({
  selector: 'app-vehicle-form',
  templateUrl: './vehicle-form.component.html',
  styleUrls: ['./vehicle-form.component.scss']
})
export class VehicleFormComponent implements OnInit {
  @Input() vehicleHistory: VehicleHistory = null;

  brandFormControl = new FormControl('', [
    Validators.required
  ]);

  modelFormControl = new FormControl('', [
    Validators.required
  ]);

  licensePlateFormControl = new FormControl('', [
    Validators.required
  ]);

  buildYearFormControl = new FormControl('', [
    Validators.required
  ]);

  colorFormControl = new FormControl('', [
    Validators.required
  ]);

  constructor(
    private accountService: AccountService
  ) { }

  ngOnInit(): void {
    this.mapVehicleToFormControlValues();
  }

  mapVehicleToFormControlValues(): void {
    if (!this.vehicleHistory) {
      return;
    }

    const {
      brand,
      model,
      licensePlate,
      buildYear,
      color
    } = this.vehicleHistory.vehicle;

    this.brandFormControl.setValue(brand);
    this.modelFormControl.setValue(model);
    this.licensePlateFormControl.setValue(licensePlate);
    this.buildYearFormControl.setValue(buildYear);
    this.colorFormControl.setValue(color);

    this.brandFormControl.disable();
    this.modelFormControl.disable();
    this.licensePlateFormControl.disable();
    this.buildYearFormControl.disable();
    this.colorFormControl.disable();
  }

  submit(formDirective: FormGroupDirective): void {

  }
}
