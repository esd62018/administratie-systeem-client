import { Component, OnInit } from '@angular/core';
import moment = require('moment');

import { AccountService } from '../../services/account/account.service';
import { VehiclesService } from '../../services/vehicles/vehicles.service';
import { SnackBarService } from '../../services/snack-bar/snack-bar.service';
import { LoaderService } from '../../services/loader/loader.service';

import { VehicleHistory } from '../../models/VehicleHistory';

import { historySorter } from '../../utils/Sort'

@Component({
  selector: 'app-vehicles',
  templateUrl: './vehicles.component.html',
  styleUrls: ['./vehicles.component.scss']
})
export class VehiclesComponent implements OnInit {
  indexOpenedVehicleHistory: number = -1;

  vehicleHistories: VehicleHistory[] = [];

  constructor(
    private accountService: AccountService,
    private vehicleService: VehiclesService,
    private snackBarService: SnackBarService,
    private loaderService: LoaderService
  ) { }

  ngOnInit(): void {
    this.getVehicleHistories();
  }

  getVehicleHistories(): void {
    this.loaderService.showLoader();

    this.vehicleService
      .getAllVehicleHistories()
      .subscribe(
        histories => this.vehicleHistories = histories.sort(historySorter),
        error => this.onError(error),
        () => this.onComplete()
      );
  }

  setOpenedVehicleHistory(number: number) {
    this.indexOpenedVehicleHistory = number;
  }

  onError(error: any): void {
    const { status } = error;
    if (status && status === 500) {
      this.snackBarService.showOops();
    }

    this.loaderService.hideLoader();
  }

  onComplete(): void {
    // this.filterHistories();
    this.loaderService.hideLoader();
  }
}
