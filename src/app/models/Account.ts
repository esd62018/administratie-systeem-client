export class Account {
  public id?: number = null;
  public username?: string = null;
  public password?: string = null;
  public role?: string = null;

  constructor(obj: Account = {} as Account) {
    const {
      id,
      username,
      password,
      role
    } = obj;

    this.id = id;
    this.username = username;
    this.password = password;
    this.role = role;
  }
}
