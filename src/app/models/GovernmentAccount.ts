export class GovernmentAccount {
  public id?: number = null;
  public username?: string = null;
  public role?: string = null;

  constructor(obj: GovernmentAccount = {} as GovernmentAccount) {
    const {
      id,
      username,
      role
    } = obj;

    this.id = id;
    this.username = username;
    this.role = role;
  }
}
