import { Journey } from './Journey';

export class Invoice {
  public id?: number = null;
  public carTracker?: string = null;
  public totalAmount?: number = null;
  public invoiceDate?: string = null;
  public paymentStatus?: string = null;
  public ownerId?: number = null;
  public category: string = null;
  public journeys: Array<Journey>;

  constructor(obj: Invoice = {} as Invoice) {
    const {
      id,
      carTracker,
      totalAmount,
      invoiceDate,
      paymentStatus,
      ownerId,
      category,
      journeys = []
    } = obj;

    this.id = id;
    this.carTracker = carTracker;
    this.totalAmount = totalAmount;
    this.invoiceDate = invoiceDate;
    this.paymentStatus = paymentStatus;
    this.ownerId = ownerId;
    this.category = category;
    this.journeys = journeys;
  }

  toString(): string {
    return 'id: ' + this.id
      + ' carTracker: ' + this.carTracker
      + ' totalAmount: ' + this.totalAmount
      + ' invoiceDate: ' + this.invoiceDate
      + ' paymentStatus: ' + this.paymentStatus
      + ' ownerId: ' + this.ownerId;
  }
}
