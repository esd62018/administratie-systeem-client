export class Owner {
  public id?: number = null;
  public citizenServiceNumber?: string = null;
  public firstName?: string = null;
  public lastName?: string = null;
  public country?: string = null;
  public city?: string = null;
  public zipCode?: string = null;
  public street?: string = null;
  public houseNumber?: string = null;

  constructor(obj: Owner = {} as Owner) {
    if (!obj) {
      return;
    }

    const {
      id,
      citizenServiceNumber,
      firstName,
      lastName,
      country,
      city,
      zipCode,
      street,
      houseNumber,
    } = obj;

    this.id = id;
    this.citizenServiceNumber = citizenServiceNumber;
    this.firstName = firstName;
    this.lastName = lastName;
    this.country = country;
    this.city = city;
    this.zipCode = zipCode;
    this.street = street;
    this.houseNumber = houseNumber;
  }

  toString(): string {
    return 'id: ' + this.id
      + ' citizenServiceNumber: ' + this.citizenServiceNumber
      + ' firstName: ' + this.firstName
      + ' lastName: ' + this.lastName
      + ' street: ' + this.street
      + ' houseNumber: ' + this.houseNumber
      + ' zipCode: ' + this.zipCode
      + ' city: ' + this.city
      + ' country: ' + this.country;
  }
}
