export class TransLocation {
  public serialNumber?: string = null;
  public lat?: string = null;
  public lon?: string = null;
  public previousLat?: string = null;
  public previousLon?: string = null;
  public dateTime?: string = null;
  public distance?: number = null;
  public price?: number = null;
  public countryCode?: string = null;

  constructor(obj: TransLocation = {} as TransLocation) {
    const {
      serialNumber,
      lat,
      lon,
      previousLat,
      previousLon,
      dateTime,
      distance,
      price,
      countryCode
    } = obj;

    this.serialNumber = serialNumber;
    this.lat = lat;
    this.previousLat = previousLat;
    this.previousLon = previousLon;
    this.lon = lon;
    this.dateTime = dateTime;
    this.distance = distance;
    this.price = price;
    this.countryCode = countryCode;
  }
}
