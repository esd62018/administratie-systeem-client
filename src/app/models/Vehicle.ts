export class Vehicle {
  public id?: number = null;
  public brand?: string = null;
  public model?: string = null;
  public licensePlate?: string = null;
  public buildYear?: number = null;
  public color?: string = null;

  constructor(obj: Vehicle = {} as Vehicle) {
    if (!obj) {
      return;
    }

    const {
      id,
      brand,
      model,
      licensePlate,
      buildYear,
      color
    } = obj;

    this.id = id;
    this.brand = brand;
    this.model = model;
    this.licensePlate = licensePlate;
    this.buildYear = buildYear;
    this.color = color;
  }

  toString(): string {
    return 'id: ' + this.id
      + '  brand: ' + this.brand
      + '  model: ' + this.model
      + '  licensePlate: ' + this.licensePlate
      + '  buildYear: ' + this.buildYear
      + '  color: ' + this.color;
  }
}
