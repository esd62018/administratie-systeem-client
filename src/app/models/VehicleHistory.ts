import { Vehicle } from './Vehicle';
import { Owner } from './Owner';

export class VehicleHistory {
  public id?: number = null;
  public fromDate?: string = null;
  public tillDate?: string = null;
  public vehicle?: Vehicle = null;
  public owner?: Owner = null;

  constructor(obj: VehicleHistory = {} as VehicleHistory) {
    if (!obj) {
      return;
    }

    const {
      id,
      fromDate,
      tillDate,
      vehicle,
      owner
    } = obj;

    this.id = id;
    this.fromDate = fromDate;
    this.tillDate = tillDate;
    this.vehicle = new Vehicle(vehicle);
    this.owner = new Owner(owner);
  }

  toString(): string {
    return 'id: ' + this.id
      + '  fromDate: ' + this.fromDate
      + '  tillDate: ' + this.tillDate
      + '  vehicle: ' + this.vehicle
      + '  owner: ' + this.owner;
  }
}
