import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';

import { HttpClientService } from '../http-client/http-client.service';
import { SnackBarService } from '../snack-bar/snack-bar.service';
import { LoaderService } from '../loader/loader.service';

import { Account } from '../../models/Account';
import * as AuthStore from '../../utils/AuthStore';


@Injectable()
export class AccountService {
  private _auth: Account = null;

  constructor(
    private http: HttpClientService,
    private router: Router,
    private snackBarService: SnackBarService,
    private loaderService: LoaderService
  ) { }

  get auth(): Account {
    return this._auth;
  }

  authenticate(): void {
    this.loaderService.showLoader();

    const { auth, token, expireDate } = AuthStore.getAuthStoreData();

    if (auth && token && expireDate) {
      this.login(new Account(auth)).subscribe(
        () => {},   // ignore (handled by login)
        () => this.snackBarService.showOops(),
        () => this.loaderService.hideLoader()
      );
    }
  }

  login(account: Account): Observable<Account> {
    return this.http
      .post<any>('auth/login', account)
      .pipe(
        tap(data => {
          if (data && data.auth && data.token && data.expireDate) {
            const { auth, token, expireDate } = data;

            this._auth = new Account(auth);

            HttpClientService.setTokenHeader(token);

            AuthStore.setAuthStoreData(token, expireDate, this._auth);

            this.router.navigate(['/dashboard', { dashboard: ['vehicles'] }]);
          }
        })
      );
  }

  // signUp(account: Account): Observable<Account> {
  //   return this.http
  //     .post('auth/sign-up', account)
  //     .pipe(
  //       tap(data => {
  //         if (data) {
  //           this.snackBarService.showSnackBar('Successfully signed up!');
  //
  //           this.router.navigate(['/login']);
  //         }
  //       })
  //     );
  // }

  update(account: Account): Observable<Account> {
    return this.http
      .put<Account>('accounts', account)
      .pipe(
        tap(data => {
          if (data) {
            this._auth = new Account(data);
          }
        })
      );
  }

  logout() {
    AuthStore.clearAuthStoreData();

    HttpClientService.clearTokenHeader();

    this.router.navigate(['/login']);
  }
}
