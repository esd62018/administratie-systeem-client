import {Injectable} from '@angular/core';
import {HttpClientService} from '../http-client/http-client.service';
import {Observable} from 'rxjs/Observable';
import {GovernmentAccount} from '../../models/GovernmentAccount';

@Injectable()
export class AccountsService {
  private apiUrl = 'accounts';

  constructor(
    private http: HttpClientService
  ) {
  }

  getAll(): Observable<GovernmentAccount[]> {
    return this.http.get<GovernmentAccount[]>(this.apiUrl);
  }

  update(account: GovernmentAccount): Observable<GovernmentAccount> {
    return this.http.put<GovernmentAccount>(this.apiUrl, account);
  }
}
