import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

import { AccountService } from '../account/account.service';

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(
    private accountService: AccountService,
    private router: Router
  ) {}

  canActivate(): boolean {
    if (this.accountService.auth !== null) {
      return true;
    }

    this.router.navigate(['/login']);

    return false;
  }
}
