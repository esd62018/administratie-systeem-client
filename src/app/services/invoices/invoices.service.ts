import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { HttpClientService } from '../http-client/http-client.service';

import { Invoice } from '../../models/Invoice';

@Injectable()
export class InvoicesService {
  private apiUrl = 'invoices';

  constructor(
    private http: HttpClientService
  ) { }

  getAllInvoices(): Observable<Invoice[]> {
    return this.http.get<Invoice[]>(this.apiUrl);
  }

  generateInvoices(date: String): Observable<Invoice[]> {
    return this.http.get<Invoice[]>(this.apiUrl + /generate/ + date);
  }

  update(invoice: Invoice): Observable<Invoice> {
    return this.http.put<Invoice>(this.apiUrl, invoice);
  }
}
