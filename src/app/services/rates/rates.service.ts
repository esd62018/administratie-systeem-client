import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { HttpClientService } from '../http-client/http-client.service';

import { Rate } from '../../models/rate';

@Injectable()
export class RatesService {
  private apiUrl = 'rates';

  constructor(
    private http: HttpClientService
  ) {
  }

  getAllRates(): Observable<Rate[]> {
    return this.http.get<Rate[]>(this.apiUrl);
  }

  addNewRate(rate: Rate): Observable<Rate> {
    return this.http.post(this.apiUrl, rate);
  }

  updateRate(rate: Rate): Observable<Rate> {
    return this.http.put(this.apiUrl, rate);
  }

  deleteRate(id: number): Observable<Rate> {
    return this.http.delete(`${this.apiUrl}/${id}`);
  }
}
