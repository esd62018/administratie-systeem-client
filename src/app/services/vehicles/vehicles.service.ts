import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { HttpClientService } from '../http-client/http-client.service';

import { Account } from '../../models/Account';
import { VehicleHistory } from '../../models/VehicleHistory';
import { Vehicle } from '../../models/Vehicle';

@Injectable()
export class VehiclesService {
  private apiUrl = 'vehicle-histories';

  constructor(
    private http: HttpClientService
  ) { }

  getAllVehicleHistories(): Observable<VehicleHistory[]> {
    return this.http.get<VehicleHistory[]>(this.apiUrl);
  }
}
