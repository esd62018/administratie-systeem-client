import { AUTH_STORE_NAMESPACE } from '../config';
import * as storage from './LocalStorage';

export const setAuthStoreData = (token, expireDate, auth) => {
  storage.setItem(AUTH_STORE_NAMESPACE, { token, expireDate, auth });
};

export const getAuthStoreData = () => {
  const data = storage.getItem(AUTH_STORE_NAMESPACE);

  if (!data) {
    return false;
  }

  return data;
};

export const clearAuthStoreData = () => {
  storage.removeItem(AUTH_STORE_NAMESPACE);
};
