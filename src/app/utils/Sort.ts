import moment = require('moment');

import { VehicleHistory } from '../models/VehicleHistory';
import { Invoice } from '../models/Invoice';
import { Rate } from '../models/Rate';

export function historySorter(a: VehicleHistory, b: VehicleHistory): number {
  const dateA = moment(a.fromDate, "DD-MM-YYYY");
  const dateB = moment(b.fromDate, "DD-MM-YYYY");

  return dateA.isBefore(dateB) ? 1 : 0;
}

export function invoiceSorter(a: Invoice, b: Invoice): number {
  return a.id <= b.id ? -1 : 1;
}

export function ratesSorter(a: Rate, b: Rate): number {
  return a.id <= b.id ? -1 : 1;
}
